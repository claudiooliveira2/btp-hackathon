import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

	posts : any;

	constructor(private firestore: AngularFirestore) {

		this.getPosts().toPromise().then((result) => {
			console.log("result", result);
		}, function(err) {
			console.log("err", err);
		});

		//console.log("posts", this.posts);

	}

	getPosts(){
    let where = undefined;

    return this.firestore.collection('fl_content').snapshotChanges()
      .pipe(
        map(posts => posts.map(this.mapPost))
      );

  }

  mapPost(post) {
  	post = post.payload.doc || post.payload;
  	console.log("post", post);
    return {
      ...post.data(),
      id: post.id
    };
  }

}
